<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright (c) 2020-2021 Heiko Bornholdt and Kevin Röbert
  ~
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy
  ~ of this software and associated documentation files (the "Software"), to deal
  ~ in the Software without restriction, including without limitation the rights
  ~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  ~ copies of the Software, and to permit persons to whom the Software is
  ~ furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  ~ EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  ~ MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  ~ IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  ~ DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
  ~ OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
  ~ OR OTHER DEALINGS IN THE SOFTWARE.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.drasyl</groupId>
    <artifactId>drasyl-parent</artifactId>
    <version>0.9.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <name>drasyl: General-Purpose Overlay Network Framework - Parent Package</name>
    <description>
        drasyl is a general-purpose overlay network framework for rapid development of distributed P2P applications.

        By using drasyl developers can fully concentrate on creating distributed applications.
        With drasyl, boundaries between IP-based networks will be eliminated and secure communication channels between any peers will be provided.
        Zero-configuration is required to use drasyl.
        Developers can run a new drasyl node without having to write configuration files or provide IP addresses of peers.
    </description>
    <url>https://drasyl.org/</url>

    <inceptionYear>2020</inceptionYear>

    <organization>
        <name>Heiko Bornholdt and Kevin Röbert</name>
        <url>https://drasyl.org/</url>
    </organization>

    <licenses>
        <license>
            <name>MIT License</name>
            <url>https://opensource.org/licenses/MIT</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <developers>
        <developer>
            <id>bornholdt</id>
            <name>Heiko Bornholdt</name>
            <email>bornholdt@informatik.uni-hamburg.de</email>
            <organization>Heiko Bornholdt and Kevin Röbert</organization>
            <organizationUrl>https://drasyl.org/</organizationUrl>
			<roles>
				<role>co-founder</role>
				<role>developer</role>
			</roles>
			<timezone>Europe/Berlin</timezone>
        </developer>
        <developer>
            <id>roebert</id>
            <name>Kevin Röbert</name>
            <email>6roebert@informatik.uni-hamburg.de</email>
            <organization>Heiko Bornholdt and Kevin Röbert</organization>
            <organizationUrl>https://drasyl.org/</organizationUrl>
			<roles>
				<role>co-founder</role>
				<role>developer</role>
			</roles>
			<timezone>Europe/Berlin</timezone>
        </developer>
    </developers>

    <modules>
        <module>drasyl-cli</module>
        <module>drasyl-core</module>
        <module>drasyl-examples</module>
        <module>drasyl-integration-tests</module>
        <module>drasyl-performance-tests</module>
        <module>drasyl-plugin-groups-client</module>
        <module>drasyl-plugin-groups-manager</module>
        <module>drasyl-all</module>
        <module>drasyl-node</module>
    </modules>

    <scm>
        <connection>scm:git:git://github.com/drasyl-overlay/drasyl.git</connection>
        <developerConnection>scm:git:ssh://git@git.informatik.uni-hamburg.de/sane-public/drasyl.git</developerConnection>
        <url>https://github.com/drasyl-overlay/drasyl</url>
        <tag>v0.7.0</tag>
    </scm>

    <issueManagement>
        <system>GitLab</system>
        <url>https://git.informatik.uni-hamburg.de/sane-public/drasyl/-/issues</url>
    </issueManagement>

    <distributionManagement>
        <repository>
            <id>ossrh</id>
            <name>OSSRH Repo</name>
            <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
        </repository>
        <snapshotRepository>
            <id>ossrh</id>
            <name>OSSRH Snapshot Repo</name>
            <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        </snapshotRepository>
    </distributionManagement>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <jdk.version>11</jdk.version>
        <java.version>11</java.version>
        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>
        <logback.version>1.2.11</logback.version>
        <junit.version>5.9.0</junit.version>
        <mockito.version>4.6.1</mockito.version>
        <jackson.version>2.13.3</jackson.version>
        <hamcrest.version>2.2</hamcrest.version>
        <awaitility.version>4.2.0</awaitility.version>
        <netty.version>4.1.79.Final</netty.version>
        <maven.scm>1.11.2</maven.scm>
        <slf4j.version>1.7.36</slf4j.version>
        <typesafe.config.version>1.4.2</typesafe.config.version>
        <jetbrains.annotations.version>20.1.0</jetbrains.annotations.version>
        <json-unit-assertj.version>2.35.0</json-unit-assertj.version>
        <protobuf.version>3.21.5</protobuf.version>
        <auto-value.version>1.9</auto-value.version>
        <jna.version>5.12.1</jna.version>

        <sonar.organization>drasyl</sonar.organization>
        <sonar.projectName>drasyl</sonar.projectName>
        <sonar.host.url>https://sonarcloud.io</sonar.host.url>
        <sonar.coverage.exclusions>
            drasyl-examples/**/*,drasyl-performance-tests/**/*
        </sonar.coverage.exclusions>
        <sonar.coverage.jacoco.xmlReportPaths>${project.artifactId}/target/site/jacoco/jacoco.xml</sonar.coverage.jacoco.xmlReportPaths>
    </properties>

    <dependencyManagement>
        <dependencies>
            <!-- Mockito -->
            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-core</artifactId>
                <version>${mockito.version}</version>
            </dependency>
            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-junit-jupiter</artifactId>
                <version>${mockito.version}</version>
            </dependency>

            <!-- JUnit -->
            <dependency>
                <groupId>org.junit.jupiter</groupId>
                <artifactId>junit-jupiter</artifactId>
                <version>${junit.version}</version>
            </dependency>
            <dependency>
                <groupId>org.junit.jupiter</groupId>
                <artifactId>junit-jupiter-api</artifactId>
                <version>${junit.version}</version>
            </dependency>
            <dependency>
                <groupId>org.junit.jupiter</groupId>
                <artifactId>junit-jupiter-engine</artifactId>
                <version>${junit.version}</version>
            </dependency>
            <dependency>
                <groupId>org.junit.jupiter</groupId>
                <artifactId>junit-jupiter-params</artifactId>
                <version>${junit.version}</version>
            </dependency>

            <!-- Testing/Hamcrest -->
            <dependency>
                <groupId>org.hamcrest</groupId>
                <artifactId>hamcrest</artifactId>
                <version>${hamcrest.version}</version>
            </dependency>

            <!-- Testing/Awaitility -->
            <dependency>
                <groupId>org.awaitility</groupId>
                <artifactId>awaitility</artifactId>
                <version>${awaitility.version}</version>
            </dependency>

            <!-- Netty -->
            <dependency>
                <groupId>io.netty</groupId>
                <artifactId>netty-transport</artifactId>
                <version>${netty.version}</version>
            </dependency>

            <dependency>
                <groupId>io.netty</groupId>
                <artifactId>netty-handler</artifactId>
                <version>${netty.version}</version>
            </dependency>

            <dependency>
                <groupId>io.netty</groupId>
                <artifactId>netty-buffer</artifactId>
                <version>${netty.version}</version>
            </dependency>

            <!-- Jackson -->
            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-databind</artifactId>
                <version>${jackson.version}</version>
            </dependency>

            <dependency>
                <groupId>com.fasterxml.jackson.dataformat</groupId>
                <artifactId>jackson-dataformat-cbor</artifactId>
                <version>${jackson.version}</version>
            </dependency>

            <!-- SLF4J -->
            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
                <version>${slf4j.version}</version>
            </dependency>

            <!-- Logback -->
            <dependency>
                <groupId>ch.qos.logback</groupId>
                <artifactId>logback-classic</artifactId>
                <version>${logback.version}</version>
            </dependency>

            <dependency>
                <groupId>net.javacrumbs.json-unit</groupId>
                <artifactId>json-unit-assertj</artifactId>
                <version>${json-unit-assertj.version}</version>
            </dependency>
            <dependency>
                <groupId>com.typesafe</groupId>
                <artifactId>config</artifactId>
                <version>${typesafe.config.version}</version>
            </dependency>
            <dependency>
                <groupId>com.google.protobuf</groupId>
                <artifactId>protobuf-java</artifactId>
                <version>${protobuf.version}</version>
            </dependency>

            <!-- Classpath Scanner -->
            <dependency>
                <groupId>io.github.classgraph</groupId>
                <artifactId>classgraph</artifactId>
                <version>4.8.149</version>
            </dependency>

            <!-- Crypto -->
            <dependency>
                <groupId>net.java.dev.jna</groupId>
                <artifactId>jna</artifactId>
                <version>${jna.version}</version>
            </dependency>

            <!-- Google Auto Value -->
            <dependency>
                <groupId>com.google.auto.value</groupId>
                <artifactId>auto-value-annotations</artifactId>
                <version>${auto-value.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <!-- Test dependencies -->
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-junit-jupiter</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-params</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <artifactId>maven-failsafe-plugin</artifactId>
                    <version>2.22.2</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-jar-plugin</artifactId>
                    <version>3.2.2</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-dependency-plugin</artifactId>
                    <version>3.3.0</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-assembly-plugin</artifactId>
                    <version>3.4.2</version>
                </plugin>
                <plugin>
                    <groupId>io.github.git-commit-id</groupId>
                    <artifactId>git-commit-id-maven-plugin</artifactId>
                    <version>5.0.0</version>
                </plugin>
                <plugin>
                    <groupId>org.sonarsource.scanner.maven</groupId>
                    <artifactId>sonar-maven-plugin</artifactId>
                    <version>3.9.1.2184</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-site-plugin</artifactId>
                    <version>3.12.1</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.10.1</version>
                </plugin>
                <plugin>
                    <groupId>com.github.spotbugs</groupId>
                    <artifactId>spotbugs-maven-plugin</artifactId>
                    <version>4.7.1.1</version>
                </plugin>
                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <version>0.8.8</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>3.0.0-M7</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <version>3.4.0</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-source-plugin</artifactId>
                    <version>3.2.1</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-deploy-plugin</artifactId>
                    <version>3.0.0</version>
                </plugin>
                <plugin>
                    <groupId>com.github.os72</groupId>
                    <artifactId>protoc-jar-maven-plugin</artifactId>
                    <version>3.11.4</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-release-plugin</artifactId>
                    <version>2.5.3</version>
                </plugin>
                <plugin>
                    <groupId>org.sonatype.plugins</groupId>
                    <artifactId>nexus-staging-maven-plugin</artifactId>
                    <version>1.6.13</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-checkstyle-plugin</artifactId>
                    <version>3.1.2</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-gpg-plugin</artifactId>
                    <version>3.0.1</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>3.3.0</version>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.sonarsource.scanner.maven</groupId>
                <artifactId>sonar-maven-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-site-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>${maven.compiler.source}</source>
                    <target>${maven.compiler.source}</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>com.github.spotbugs</groupId>
                <artifactId>spotbugs-maven-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                        <configuration>
                            <propertyName>surefireArgLine</propertyName>
                        </configuration>
                    </execution>
                    <execution>
                        <id>report</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
                <executions>
                    <execution>
                        <id>default-deploy</id>
                        <phase>deploy</phase>
                        <goals>
                            <goal>deploy</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <!-- Currently not supported by JUnit5: https://github.com/junit-team/junit5/issues/1424#issuecomment-390313944 -->
                    <!--<parallel>all</parallel>-->
                    <!-- Sets the VM argument line used when unit tests are run. -->
                    <!--suppress MavenModelInspection -->
                    <argLine>-Dio.netty.leakDetectionLevel=PARANOID ${surefireArgLine}</argLine>
                    <runOrder>random</runOrder>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <doclint>none</doclint>
                    <source>11</source>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <configuration>
                    <tagNameFormat>v@{project.version}</tagNameFormat>
                    <localCheckout>true</localCheckout>
                    <mavenExecutorId>forked-path</mavenExecutorId>
                    <useReleaseProfile>false</useReleaseProfile>
                    <releaseProfiles>release</releaseProfiles>
                    <goals>deploy</goals>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <configuration>
                    <consoleOutput>true</consoleOutput>
                    <logViolationsToConsole>true</logViolationsToConsole>
                    <configLocation>checkstyle.xml</configLocation>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <version>3.1.0</version>
                <configuration>
                    <rules>
                        <dependencyConvergence />
                    </rules>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <reporting>
        <plugins>
            <plugin>
                <groupId>com.github.spotbugs</groupId>
                <artifactId>spotbugs-maven-plugin</artifactId>
                <version>4.7.1.1</version>
                <configuration>
                    <effort>Max</effort>
                    <threshold>Low</threshold>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <version>3.1.2</version>
                <configuration>
                    <configLocation>checkstyle.xml</configLocation>
                </configuration>
            </plugin>
        </plugins>
    </reporting>

    <profiles>
        <profile>
            <id>release</id>
            <build>
                <plugins>
                    <!-- To sign the artifacts -->
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-release-plugin</artifactId>
                        <configuration>
                            <pushChanges>false</pushChanges>
                        </configuration>
                        <dependencies>
                            <dependency>
                                <groupId>org.apache.maven.scm</groupId>
                                <artifactId>maven-scm-provider-gitexe</artifactId>
                                <!--suppress MavenModelInspection -->
                                <version>1.13.0</version>
                            </dependency>
                        </dependencies>
                    </plugin>
                    <plugin>
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <extensions>true</extensions>
                        <configuration>
                            <serverId>ossrh</serverId>
                            <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                            <autoReleaseAfterClose>true</autoReleaseAfterClose>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>api</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>aggregate</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <sourceFileExcludes>
                                <exclude>**/example/**/*.java</exclude>
                                <exclude>**/org/drasyl/cli/**/*.java</exclude>
                                <exclude>**/org/drasyl/test/**/*.java</exclude>
                            </sourceFileExcludes>
                            <doctitle>drasyl API Reference (${project.version})</doctitle>
                            <windowtitle>drasyl API Reference (${project.version})</windowtitle>
                            <groups>
                                <group>
                                    <title>Central interface for all I/O operations</title>
                                    <packages>org.drasyl.channel*</packages>
                                </group>
                                <group>
                                    <title>Reusable I/O event interceptors</title>
                                    <packages>org.drasyl.handler*</packages>
                                </group>
                                <group>
                                    <title>Utils</title>
                                    <packages>org.drasyl.util*</packages>
                                </group>
                                <group>
                                    <title>Plugins</title>
                                    <packages>org.drasyl.plugin*</packages>
                                </group>
                                <group>
                                    <title>Node</title>
                                    <packages>org.drasyl.node*</packages>
                                </group>
                            </groups>
                            <locale>en_US</locale>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
