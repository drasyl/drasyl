# Public Super Peers

Exposed drasyl nodes, configured as super peers and acting as rendezvous servers and relays, are required for discovery of other nodes.

We run some public super peers, so you don't have to.

| **Endpoint**  | **Used drasyl version**  |  
|---------------|--------------------------|
| ```udp://sp-fra1.drasyl.org:22527?publicKey=c0900bcfabc493d062ecd293265f571edb70b85313ba4cdda96c9f77163ba62d&networkId=1``` | Latest stable [release](https://github.com/drasyl-overlay/drasyl/releases) | 
| ```udp://sp-nbg2.drasyl.org:22527?publicKey=5b4578909bf0ad3565bb5faf843a9f68b325dd87451f6cb747e49d82f6ce5f4c&networkId=1``` | Latest stable [release](https://github.com/drasyl-overlay/drasyl/releases) | 

By default, all drasyl nodes are configured to use the super peers `sp-fra1.drasyl.org`
and `sp-nbg2.drasyl.org`.

